# Vite + Vuetify generator

Yeoman generator for Vite + Vuetify project.

```
npm i -g yeoman-generator
npm i -g generator-vite-vuetify
yo vite-vuetify
```

### Release

This repository is configured to release a new version each commit on main branch. Beta release will occour each commit on next branch.
