'use strict';

import Generator from 'yeoman-generator';
import { globSync } from 'glob';
import path from 'path';
import _ from 'lodash';

export default class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.option('name', {
      description: 'The name of your app',
      type: String,
    });
  }

  async prompting() {
    this.log('Prompting questions');
    this.answers = await this.prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Your project name',
        default: this.appname,
        when: () => !this.options.name,
      },
      {
        type: 'input',
        name: 'git',
        message: 'You want to use Git?',
        default: false,
      },
    ]);
    this.options.name = this.answers.name || this.options.name;
  }

  writing() {
    this.log('Writing files');
    this.destinationRoot(this.options.name);

    const files = globSync('**/*', { cwd: this.sourceRoot(), nodir: true, dot: true, posix: true });
    const tplFiles = [
      'index.html',
      'README.md',
      'package.json',
      '.husky/pre-commit',
      'src/App.vue',
    ];

    const ignores = ['.DS_Store'];
    const vsCodeFiles = ['.vscode/extensions.json'];
    const tsFiles = [
      'tsconfig.json',
      'tsconfig.app.json',
      'tsconfig.node.json',
      'tsconfig.vitest.json',
    ];

    const templateData = {
      name: this.options.name,
    };

    files.forEach((file) => {
      // Ignore files.
      if (_.indexOf(ignores, file) !== -1) {
        return;
      }

      const ext = path.extname(file).substring(1);
      const fileWithoutExt = file.substring(0, file.length - ext.length - 1);
      const sourcePath = this.templatePath(file);
      let destinationPath = this.destinationPath(file);

      if (tplFiles.indexOf(file) !== -1) {
        this.fs.copyTpl(sourcePath, destinationPath, templateData);
        return;
      }

      this.log('Copied without template');
      this.fs.copy(sourcePath, destinationPath);
    }, this);
    this.env.cwd = this.destinationPath();
    this.addDependencies('vuetify', 'vue-i18n');
    this.addDevDependencies('husky');
    this.log('Updated package.json');
  }

  install() {
    this.log('Inside install step');
  }

  end() {
    this.log('Ending');
    if (this.answers.git !== false) {
      this.spawn('git', ['init']);
      this.spawn('npx', ['husky', 'init']);
      this.log('Installed git and husky');
    }
    this.log('Ended');
  }
}
