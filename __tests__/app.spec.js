import { describe, it } from 'mocha';
import helpers from 'yeoman-test';
import assert from 'yeoman-assert';
import path from 'path';

import { fileURLToPath } from 'url';
import { dirname } from 'path';

import fs from 'fs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

function getHelper() {
  return helpers.run(path.join(__dirname, '..', 'generators', 'app'));
}

describe('App template', function () {
  it('Contains README with app name', function () {
    const name = 'test-app';
    return getHelper()
      .withAnswers({ name })
      .then(function () {
        assert.fileContent(`${name}/README.md`, /# test-app/);
        assert.fileContent(`${name}/package.json`, /"name": "test-app"/);
      });
  });

  it('contains husky', function () {
    const name = 'test';
    return getHelper()
      .withAnswers({ name, git: false })
      .then(function (dir) {
        console.log('DIR: ', dir.cwd);
        console.log(fs.readdirSync(dir.cwd));
        assert.fileContent(`${name}/README.md`, /# test/);
        // Hidden files are not listed in this environment, we need further investigation.
        assert.noFile(`${name}/.git`);
        // assert.fileContent(".husky/pre-commit", "npm test");
      });
  });
});
