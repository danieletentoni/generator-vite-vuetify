/** @type {import("prettier").Config} */
// Look at the .editorconfig too.
export default {
  printWidth: 100,
  semi: true,
  singleAttributePerLine: true,
  singleQuote: true,
  trailingComma: 'all',
  vueIndentScriptAndStyle: true,
};
